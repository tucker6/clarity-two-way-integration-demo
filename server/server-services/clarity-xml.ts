import axios from 'axios';
import fs from 'fs';
import FormData from 'form-data';
import { AxiosRequestConfig } from 'axios';
import { xml2json } from 'xml-js';
import { DITTemplate } from '../resources/xml-upload-template';


export interface SourceInfo {
      // Source information.
      SourceID: string;
      SourceType: number;
      SourceName: string;
      SoftwareName: string;
      SoftwareVersion: string;

      // User
      UserID: number;
      SourceContactEmail: string;
      SourceContactFirstName: string;
      SourceContactLastName: string;
}

export interface ExportInfo {
    ExportID: number;
    ExportDate: string;
    ExportStartDate: string;
    ExportEndDate: string;
}

export interface Profile {
    PersonalID: number;
    FirstName: string;
    LastName: string;
    NameDataQuality?: number;
    SSN: number;
    SSNDataQuality?: number;
    DOB: string;
    DOBDataQuality?: number;
    Race?: number;
    Ethnicity?: number;
    Gender?: number;
    VeteranStatus?: number;
    c_covid_19_test_result?: number;
    covid_test_date?: string;
}

export class DIT {

    apiUrl = '';
    templatesPath = '';

    sourceInfo: SourceInfo;

    constructor(instanceName: string, templateSettingsPath: string = './project_settings/') {
      this.apiUrl = `https://api-${instanceName}.clarityhs.com/import/xml`;
      try {
          this.sourceInfo = this.setSourceInfoFromJSONFile(templateSettingsPath + 'sourceInfo.json');
      } catch (e) {
        console.log(e);
      }
    }

    private setSourceInfoFromJSONFile(path: string): SourceInfo {
        return JSON.parse(fs.readFileSync(path).toString()) as SourceInfo;
    }

    private createRandomId(max: number = 99999999999): number {
        return Math.floor(Math.random() * Math.floor(max));
    }

    private addHMISTags(xml: object): string {
        let taggedXMLClient = '';
        for (let [key, value] of Object.entries(xml)) {
            // TODO: Add in all hashStatus fields.
            if (key === 'SSN' && typeof value === 'string'){ value = value.replace(/-/g, ''); }
            if (['FirstName', 'LastName', 'SSN'].includes(key)) {
              taggedXMLClient += `\t\t<hmis:${key} hmis:hashStatus="1">${value}</hmis:${key}>\n`;
            } else {
              taggedXMLClient += `\t\t<hmis:${key}>${value}</hmis:${key}>\n`;
            }
        }
        return taggedXMLClient;
    }

    public createUpload(profile: Profile): string {

        const exportInfo = {
            ExportID: this.createRandomId(),
            ExportDate: new Date().toISOString().slice(0, 19),
            ExportStartDate: new Date().toISOString().slice(0, 19),
            ExportEndDate: new Date().toISOString().slice(0, 19)
        } as ExportInfo;

        // Add HMIS tags.
        const profileXML = this.addHMISTags(profile);

        const template = DITTemplate.getTemplate(this.sourceInfo, exportInfo, profileXML);
        return template;
    }


    public async uploadXML(apiLogin: string, apiSecret: string, xml: string): Promise<object> {
        return new Promise((resolve, reject) => {

            const headers = {
                'Content-Type': 'application/xml',
                'Standard-Version': '2020',
                'Content-Encrypted': '0'
            };

            const formData = new FormData();
            formData.append('files', Buffer.from(xml));

            axios({method: 'post',
                  url: this.apiUrl + '/data',
                  headers,
                  auth: {
                    username: apiLogin,
                    password: apiSecret
                  },
                  data: Buffer.from(xml)
            } as AxiosRequestConfig).then((response) => {
                const options = {compact: true, ignoreComment: true, spaces: 4};
                const resJson = JSON.parse(xml2json(response.data, options)).response;
                resolve(resJson);
            })
            .catch((e) => {
                reject(e);
            });
        });
    }
}

