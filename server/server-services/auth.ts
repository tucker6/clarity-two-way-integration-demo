// System libaries.
import { exit } from 'process';
import fs from 'fs';

import axios from 'axios';

export interface LookerCreds {
    lookerUserName: string;
    lookerSecret: string;
}

export interface DITCreds {
    ditApiLogin: string;
    ditApiKey: string;
}

export interface Credentials {
    looker: LookerCreds;
    dit: DITCreds;
}

export class AuthenticateLooker {

    private lookerBaseUrl: string;
    private lookerId: string;
    private lookerSecret: string;
    private expiredToken = false;

    public accessToken: string;


    constructor(lookerBaseUrl: string, lookerId: any, lookerSecret: any){
        this.lookerBaseUrl = lookerBaseUrl;
        this.lookerId = lookerId;
        this.lookerSecret = lookerSecret;

        this.login().then((result: any) => {
          this.accessToken = result.token;
        }).catch((err) => {
          console.log(err);
        });
    }

    public getAccessToken(): string {
        if (this.expiredToken){
            this.login().then((result: any) => {
                this.accessToken = result.token;
                return this.accessToken;
            }).catch((err) => {
                return '';
            });
        } else {
            return this.accessToken;
        }
    }

    private async login(): Promise<object> {
        const url = this.lookerBaseUrl + `login?client_id=${this.lookerId}&client_secret=${this.lookerSecret}`;
        const headers = {
            'Content-Type': 'application/json;charset=utf-8'
        };

        try {
            const result = await axios({
                method: 'post',
                url,
                headers
            });

            this.expiredToken = false;
            setTimeout(() => {
                this.expiredToken = true;
            }, result.data.expires_in * 1000); // Timeout is in milliseconds.

            return {token: result.data.access_token };
        } catch (e) {
            return e;
        }
    }
}

export class Credentials {

    public static get(credentialsPath: string): Credentials {
        try {

            // Retrieve credentials from file.
            const creds = JSON.parse(fs.readFileSync(credentialsPath).toString()) as Credentials;

            if (!creds.dit.ditApiLogin || !creds.dit.ditApiKey){ throw new Error(('Error')); }
            if (!creds.looker.lookerUserName || !creds.looker.lookerSecret){ throw new Error(('Error')); }

            return creds;
          } catch (e) {
            console.log('Failed to parse Looker or DIT credentials.  Exiting.');
            exit();
          }
    }
}
