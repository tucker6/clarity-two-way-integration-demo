import { ExportInfo, Profile, SourceInfo } from 'server/server-services/clarity-xml';

export class DITTemplate {

  static getTemplate(sourceInfo: SourceInfo, exportInfo: ExportInfo, profile: string): string {
    return `<?xml version="1.0" encoding="UTF-8"?>
    <hmis:Sources xmlns:airs="http://www.clarityhumanservices.com/schema/2020/1/AIRS_3_0_mod.xsd"
                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
                  xmlns:hmis="http://www.clarityhumanservices.com/schema/2020/1/HUD_HMIS.xsd">
    <hmis:Source>
      <hmis:SourceID>${sourceInfo.SourceID}</hmis:SourceID>
      <hmis:SourceType>${sourceInfo.SourceType}</hmis:SourceType>
      <hmis:SourceName>${sourceInfo.SourceName}</hmis:SourceName>
      <hmis:SoftwareName>${sourceInfo.SoftwareName}</hmis:SoftwareName>
      <hmis:SoftwareVersion>${sourceInfo.SoftwareVersion}</hmis:SoftwareVersion>
      <hmis:SourceContactEmail>${sourceInfo.SourceContactEmail}</hmis:SourceContactEmail>
      <hmis:SourceContactFirst>${sourceInfo.SourceContactFirstName}</hmis:SourceContactFirst>
      <hmis:SourceContactLast>${sourceInfo.SourceContactLastName}</hmis:SourceContactLast>
      <hmis:Export>
      <hmis:ExportID>${exportInfo.ExportID}</hmis:ExportID>
      <hmis:ExportDate>${exportInfo.ExportDate}</hmis:ExportDate>
      <hmis:ExportPeriod>
        <hmis:StartDate>${exportInfo.ExportStartDate}</hmis:StartDate>
        <hmis:EndDate>${exportInfo.ExportEndDate}</hmis:EndDate>
      </hmis:ExportPeriod>
      <hmis:ExportPeriodType>reportingPeriod</hmis:ExportPeriodType>
      <hmis:ExportDirective>deltaRefresh</hmis:ExportDirective>

      <hmis:Client hmis:dateCreated="${new Date().toISOString().slice(0, 19)}"
                    hmis:dateUpdated="${new Date().toISOString().slice(0, 19)}"
                    hmis:userID="${sourceInfo.UserID}">
        ${profile}
      </hmis:Client>
      </hmis:Export>
    </hmis:Source>
    </hmis:Sources>`;
  }
}
