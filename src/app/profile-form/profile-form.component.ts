import { Component, Injectable, OnInit, OnChanges, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment.prod';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.css']
})
@Injectable()
export class ProfileFormComponent implements OnInit {

  @ViewChild('stepper') stepper: any;

  profileData = {};

  ethnicities = [
    { name: 'Non-Hispanic/Non-Latino', value: 0 },
    { name: 'Hispanic/Latino', value: 1 }
  ];
  genders = [
    { name: 'Female', value: 0 },
    { name: 'Male', value: 1 },
    { name: 'Trans Female (MTF or Male to Female)', value: 2 },
    { name: 'Trans Male (FTM or Female to Male)', value: 3 },
    { name: 'Gender Non-Conforming (i.e. not exclusively male or female)', value: 4 }
  ];
  veteranStatuses = [
    { name: 'No', value: 0 },
    { name: 'Yes', value: 1 }
  ];
  covidStatuses = [
    { name: 'No', value: 0 },
    { name: 'Yes', value: 1 }
  ];

  // Define UI "Profile" form object
  profileForm = this.fb.group({
    PersonalID: [null],
    FirstName: ['Michael', Validators.required],
    LastName: ['Scottt', Validators.required],
    NameDataQuality: [1],
    SSN: ['123-45-9900', Validators.pattern(/^(?!219-09-9999|078-05-1120)(?!666|000|9\d{2})\d{3}-(?!00)\d{2}-(?!0{4})\d{4}$/)],
    SSNDataQuality: [1],
    DOB: ['1974-10-02', Validators.required],
    DOBDataQuality: [1],
    Race: [5],
    Ethnicity: [0],
    Gender: [1, Validators.required],
    VeteranStatus: [0],
    c_covid_19_test_result: [1],
    covid_test_date: [new Date().toISOString().slice(0, 10)]
  });

  // Progress bar.
  loading = false;

  // Verified as homeless.
  verifiedHomeless = false;

  constructor(private fb: FormBuilder, private http: HttpClient) {}

  ngOnInit(): void {
    this.onChanges();
  }

  onChanges(): void {
    this.profileForm.valueChanges.subscribe(val => {
      this.profileData = val;
    });
  }

  searchClients(): void {
    // Specify JSON as the return type from the Data Analysis API.
    const resultFormat = 'json';

    // Define the search query to send to the Data Analysis API.
    const body = {
      model: 'demo_connection_model',
      view: 'client_model',
      fields: ['client_model.id'],
      limit: 1,
      filters: {
        'client_model.first_name': this.profileForm.get('FirstName').value,
        'client_model.last_name': this.profileForm.get('LastName').value,
        'client_model.ssn': this.profileForm.get('SSN').value,
        'client_model.birth_date': this.profileForm.get('DOB').value
      }
    };

    // Show progress bar
    this.loading = true;

    // Search for the client.
    this.http.post<any>(`${environment.apiUrl}/looker-api/queries/run/${resultFormat}`, body).subscribe(
      response => {
        this.loading = false;

        if (response[0]) {
          // Set the PersonalID value in the form now that we have the client ID from Clarity
          this.profileForm.get('PersonalID').setValue(response[0]['client_model.id']);
        }

        // Check if the client was found and update the verifiedHomeless property accordingly
        if (response.length > 0) {
          this.verifiedHomeless = true;
          setTimeout(() => { this.stepper.next(); }, 2200); // Delay to let the user know what's going on.
        } else {
          this.verifiedHomeless = false;
          setTimeout(() => { this.stepper.previous(); }, 3600);
        }
      },
      error => {
        console.log(error);
      });
  }

  // Save the data to Clarity.
  uploadClient(): void {
    // Remove dashes from SSN to upload through DIT
    this.removeSSNDashes();

    // For security reasons, drop the request to our Node server.
    this.http.post(`http://localhost:2048/clients`, this.profileForm.value).subscribe(response => {
      console.log(response);
      this.profileForm.reset();
      setTimeout(() => { this.stepper.selectedIndex = 0; }, 500);
    });
  }

  formatSSN(event: any): void {
    let val = this.profileForm.get('SSN').value;
    if (val === '' || val === null){ return; }
    val = val.replace(/\D/g, '');
    let newVal = '';
    if (val.length > 4) {
      val = val;
    }
    if ((val.length > 3) && (val.length < 6)) {
       newVal += val.substr(0, 3) + '-';
       val = val.substr(3);
    }
    if (val.length > 5) {
       newVal += val.substr(0, 3) + '-';
       newVal += val.substr(3, 2) + '-';
       val = val.substr(5);
    }
    newVal += val;
    val = newVal.substring(0, 11);
    this.profileForm.get('SSN').setValue(val);
  }

  private removeSSNDashes(): void {
    let ssn = this.profileForm.get('SSN').value;
    if (ssn){
      ssn = ssn.replace(/-/g, '');
      this.profileForm.get('SSN').setValue(ssn);
    }
  }
}
