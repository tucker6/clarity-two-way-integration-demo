#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Sep 17 13:53:55 2020
Updated: 2021-02-23

@author: C. Thomas Brittain
"""

import requests

##############
# Parameters
##############

XML_PATH        = "PATH_TO_FILE"
API_LOGIN       = "YOUR_API_LOGIN"
API_KEY         = "YOUR_API_PASSWORD"
API_URL         = "https://api-INSTANCE_NAME.clarityhs.com/import/xml/"

IS_MAP_DOC      = False

#############
# Setup
#############

url = API_URL + "map" if IS_MAP_DOC else API_URL + "data"

# Add headers to notify API data type
headers = {
    "Content-Type":"text/xml",
    "Standard-Version": "2020",
    "Content-Encrypted": "0"    
}

# Upload
r = requests.post(url,
                  auth=(API_LOGIN, API_KEY),
                  verify = False,
                  headers=headers,
                  data=open(XML_PATH, 'rb').read()
)

# Check Status
print(r.text)
