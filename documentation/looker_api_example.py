import os
import json
import requests
from io import StringIO
import pandas as pd

from looker import LookerServices

###############
# Authenticate
###############
# Clarity Instance Parameters
looker_url = "https://looker.clarityhs.com:19999/api/3.1"

root_path = os.environ["HOME"]

# API Credentials
# Pull credentials from a safely stored JSON file.
# DO NOT STORE CREDENTIALS IN SCRIPT.
creds = dict()
with open(f"{root_path}/.bitfocus/config.json") as f:
    creds = json.load(f)

looker = LookerServices()
looker.log_me_in(creds["looker"]["LOOKER_USER_NAME"], creds["looker"]["LOOKER_SECRET"])

# Login
payload = {
    "client_id": creds["looker"]["LOOKER_USER_NAME"], 
    "client_secret": creds["looker"]["LOOKER_SECRET"]
}

login_url = f"{looker_url}/login"
access_token = requests.post(login_url, data=payload).json()['access_token']
headers = {
    "Authorization": "token " + access_token,
    "ContentType": "application/json"
}

#############
# Send Query
#############
# Define your query

# view: base = HMIS Performance
# view: client = Coordinated Entry
# view: clients = Services
# view: client_model = Client Model
# view: data_quality = Data Quality Model
# view: agencies = Project Descriptor Model
# view: 

query = {
    "model": "demo_connection_model",
    "view": "client",                         
    "fields": [
        "clients.id",
        "static_demographics.veteran_text",
        "clients.added_date"
    ],
    "filters": {"clients.added_date": "after 2020-01-01"},
    "limit": -1
}

################
# Get Response
################
# Run query
data = requests.post(f"{looker_url}/queries/run/csv", json=query, headers=headers)

# Convert to CSV.
df = pd.read_csv(StringIO(data.text))

